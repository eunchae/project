# -*- coding: utf-8 -*-

import datetime
import pymysql

conn = pymysql.connect(host='localhost', user='root', password='jinong1234%',
               db='library', charset='utf8mb4')

curs = conn.cursor()

Num = 1
UserList = []
BookList = []
LoanList = []
OverdueList = []

menus = [
        {
            'name': '고객',
            'menu': [
                '고객 리스트', '고객 등록', '고객 삭제'
            ]
        },
        {
            'name': '책',
            'menu': [
                '책 리스트', '책 등록', '책 삭제'
            ]
        },
        {
            'name': '대여',
            'menu': [
                '대여 리스트', '연체 리스트', '신규 대여', '대여 반납'
            ]
        }
    ]


def makeList(typelist):
    curs.execute(sql)
    data = curs.fetchall()
    print data
    count = len(data)
    for i in range(count):
        typelist.append(data[i][1])

def showList(typelist):
    print(typelist)

def addUser():
    print"고객 추가"
    user = input("고객 이름을 등록해주세요:")
    if user in UserList :
        print("이미 등록된 고객입니다.")
    else :
        phone = input("고객 핸드폰 번호를 등록해주세요:")
        sql = "INSERT INTO user(name, phone) Value (%s, %s)"
        try:
            curs.execute(sql, (user,phone))
            conn.commit()
            UserList.append(user)
            print user, "가 추가되었습니다"
        except:
            print "db err"

def removeUser():
    print "고객삭제"
    user = input("삭제할 고객의 이름을 적어주세요")
    if user not in UserList:
        print("존재하지않는 고객입니다.")
    else :
        for i in range(len(LoanList)):
            if user == LoanList[i][0] :
                print("현재 대여중이라 삭제할 수 없습니다.")
            else:
                sql = "DELETE FROM user where name = %s"
                try:
                    curs.execute(sql, user)
                    conn.commit()
                    UserList.remove(user)
                    print user, "가 삭제되었습니다."
                except:
                    print "db err"

def addBook():
    print "책 추가"
    book = input("책 이름을 등록해주세요:")
    if book in BookList :
         print("이미 등록된 책입니다.")
    else :
        sql = "INSERT INTO book(name) Value (%s)"
        try:
            curs.execute(sql, book)
            conn.commit()
            BookList.append(book)
            print book, "가 추가되었습니다"
        except:
            print "db err"


def removeBook():
    print "책삭제"
    book = input("삭제할책의 이름을 적어주세요")
    if book not in BookList:
        print("존재하지않는 책입니다.")
    else :
        for i in range(len(LoanList)):
            if book == LoanList[i][1] :
                print("현재 대여중이라 삭제할 수 없습니다.")
            else:
                sql = "DELETE FROM book where name = %s"
                try :
                    curs.execute(sql, book)
                    conn.commit()
                    BookList.remove(book)
                    print book, "가 삭제되었습니다"
                except:
                    print "db err"

def makeListLoan():
    sql = "select * from loan"
    curs.execute(sql)
    data = curs.fetchall()
    count = len(data)
    for i in range(count):
        LoanList.append([data[i][1],data[i][2]])
        temp = data[i][3]
        startdate = datetime.date(temp.year, temp.month, temp.day)
        today = datetime.date.today()
        diff = (today - startdate).days
        if diff > 7 :
            OverdueList.append(data[i][1])
        else :
            pass

def showBorrow():
    for i in range(len(LoanList)):
        print "고객 : ", LoanList[i][0], "\t책 : ", LoanList[i][1]

def newBorrow():
    temp = False
    print "신규대여"
    user = input("대여할 회원의 이름을 입력해주세요 : ")
    if user in UserList :
        if user not in OverdueList:
            sql = "SELECT * FROM loan where user_no = %s"
            curs.execute(sql, user)
            data = curs.fetchall()
            count = len(data)
            if count < 3 :  #3권 이상으로 수정
                 book = input("대여할 책 이름을 입력해주세요 :")
                 if book in BookList :
                     for i in range(len(LoanList)):
                         if book == LoanList[i][1]:
                             temp = True
                             break

                     if temp != True :
                         sql = "INSERT INTO loan(user_no, book_no) Value (%s, %s)"
                         try:
                             curs.execute(sql, (user,book))
                             conn.commit()
                             LoanList.append([user,book])
                             print user, "가",book, "을 대여하였습니다"
                         except:
                             print "db err"

                 else :
                    print("존재하지 않는 책입니다.")
            else :
                 print("더 이상 책을 빌릴 수 없습니다.")
        else :
            print("연체중인 고객입니다.")
    else :
        print("존재하지 않는 회원입니다.")


def returnBook():
    print "대여반납"
    user = input("반납할 고객의 이름을 입력해주세요 :")
    for i in range(len(LoanList)):
        if user == LoanList[i][0]:
            book = input("반납할 책의 이름을 입력해주세요 : ")
            if book == LoanList[i][1]:
                sql = "DELETE FROM loan where user_no = %s and book_no = %s "
                try:
                    curs.execute(sql, (user,book))
                    conn.commit()
                    LoanList.remove([user,book])
                    print user,"가", book,"을 반납했습니다."
                    if user in OverdueList :
                        OverdueList.remove(user)
                except:
                    print "db err"
            else:
                print("책 이름을 잘못입력하셨습니다.")
            break
        else:
            print("회원 이름을 잘못입력하셨습니다.")

def User() :
    global Num
    while 1 :
        print '{}\n 1.{} \n 2.{} \n 3.{}\n\n'.format(menus[0]['name'],menus[0]['menu'][0],menus[0]['menu'][1],menus[0]['menu'][2])
        Num = input("\n메뉴를 선택해주세요(메인메뉴:0) : ")
        if Num == 1 :
            print "\n고객리스트\n"
            showList(UserList)
        elif Num == 2 :
            addUser()
        elif Num == 3:
            removeUser()
        elif Num == 0 :
            break
        else :
            print("\n잘못된 번호입니다.")

def Book() :
    global Num
    while 1 :
        print '{}\n 1.{} \n 2.{} \n 3.{}\n\n'.format(menus[1]['name'],menus[1]['menu'][0],menus[1]['menu'][1],menus[1]['menu'][2])
        Num = input("\n메뉴를 선택해주세요(메인메뉴:0) : ")

        if Num == 1 :
            print "\n책리스트\n"
            showList(BookList)
        elif Num == 2 :
            addBook()
        elif Num == 3:
            removeBook()
        elif Num == 0 :
            break
        else :
            print "\n잘못된 번호입니다."

def Borrow() :
    global Num
    while 1 :
        print '{}\n 1.{} \n 2.{} \n 3.{} \n 4.{}\n\n'.format(menus[2]['name'],menus[2]['menu'][0],menus[2]['menu'][1],menus[2]['menu'][2],menus[2]['menu'][3])
        Num = input("\n메뉴를 선택해주세요(메인메뉴:0) : ")

        if Num == 1 :
            print "\n대여 리스트\n"
            showBorrow()
        elif Num == 2 :
            print "\n연체리스트\n"
            showList(OverdueList)
        elif Num == 3:
            newBorrow()
        elif Num == 4:
            returnBook()
        elif Num == 0 :
            break
        else :
            print("\n잘못된 번호입니다.")

def Menu():
    global Num
    while 1 :
        print '\n 1.{} \n 2.{} \n 3.{}\n\n'.format(menus[0]['name'],menus[1]['name'],menus[2]['name'])
        Num = input("\n메뉴를 선택하세요 : (종료:0)")

        if Num == 1 :
            User()
        elif Num == 2 :
            Book()
        elif Num == 3 :
            Borrow()
        elif Num == 0 :
            print("종료")
            break
        else :
            print("잘못입력하셨습니다.\n")


makeListLoan()
sql = "select * from user"
makeList(UserList)
sql = "select * from book"
makeList(BookList)
print LoanList
Menu()
